# Git commands 
There are around 200+ commands, but following commands are mostly used:

 1. `git init : create a local repo ` 
 2. `git clone :sync a remote repo to local `
 3. `git commit : create a new version`
 4. `git log : information about different versions`
 5. `git status : status of files` In git commit, provide a small message, a brief information about what has changed since last commit.
